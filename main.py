

from __future__ import print_function

from flask import Flask, render_template
from jinja2 import Template
from flask_sqlalchemy import SQLAlchemy

from os import path
import sys
import io

from models import sqldb
import datetime

from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy import MetaData
from sqlalchemy import Table

import time
import grovepi
from picamera import PiCamera

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config["CACHE_TYPE"] = "null"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False 
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://DB_USER:postgres@postgres/test'

'''
POSTGRES = {
    'user': 'postgres',
    'pw': 'postgres',
    'db': 'professional_maintenance',
    'host': 'localhost',
    'port': '5432',
}
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
'''

'''
POSTGRES = {
    'user': 'root',
    'pw': 'root',
    'db': 'webmvcframework10',
    'host': 'localhost',
    'port': '3306',
}
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
'''

POSTGRES = {
    'user': 'u733092923_kms',
    'pw': 'kmsnota2018',
    'db': 'u733092923_kms',
    'host': 'sql122.main-hosting.eu',
    'port': '3306',
}

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES


sqldb.init_app(app)
engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])

# change to "redis" and restart to cache again

d = path.dirname(__file__)

#nltk.download('punkt')

###SENSORS STUFF

#camera instance
camera = PiCamera()

# Connect the Grove Sound Sensor to analog port A0
# SIG,NC,VCC,GND
sound_sensor = 0

# Connect the Grove Light Sensor to analog port A0
# SIG,NC,VCC,GND
light_sensor = 1

# Connect the Grove Ultrasonic Ranger to digital port D4
# SIG,NC,VCC,GND
ultrasonic_ranger = 4

# Connect the Grove LED to digital port D5
# SIG,NC,VCC,GND
led = 5

button = 6


grovepi.pinMode(sound_sensor,"INPUT")
grovepi.pinMode(light_sensor,"INPUT")
grovepi.pinMode(button,"INPUT")

grovepi.pinMode(led,"OUTPUT")


Base = automap_base()

insp = reflection.Inspector.from_engine(engine)
print(insp.get_table_names())

# reflect the tables
Base.prepare(engine, reflect=True)

tables = ['access_level', 'attribute', 'camera_images', 'light_sensor', 'part', 'part_attributes', 'part_type', 'part_types_attributes', 'sound_sensor', 'ultrasonic_ranger', 'user']


Attribute,Part = Base.classes.attribute, Base.classes.part

PartAttributes = Base.classes.part_attributes

SoundSensors = Base.classes.sound_sensor

LightSensors = Base.classes.light_sensor

CameraSensors = Base.classes.camera_images

UltrasoundSensor = Base.classes.ultrasonic_ranger

session = Session(engine)


to_send1 = []
to_send2 = []
to_send3 = []
to_send4 = []

counter = 0
while True:
    try:


        #camera.start_preview()
        #camera.capture('/home/pi/Desktop/image%s.jpg' % i)
        #camera.stop_preview()
        # Get sensor values
        sound_sensor_value = grovepi.analogRead(sound_sensor)
        light_sensor_value = grovepi.analogRead(light_sensor)
        ranger_sensor_value = grovepi.ultrasonicRead(ultrasonic_ranger)
        button_value = grovepi.digitalRead(button)
        #SOUND SENSOR DATA
        print("sound_sensor_value = %d" %sound_sensor_value)
        #time.sleep(.1)

        #LIGHT SENSOR DATA
        # Calculate resistance of sensor in K
        resistance = (float)(1023 - light_sensor_value) * 10 / light_sensor_value
        print("Light_sensor_value = %d resistance = %.2f" %(light_sensor_value,  resistance))
        #time.sleep(.1)

        #ULTRASOUND SENSOR DATA
        print("ultrasound_sensor_value = ",ranger_sensor_value)
        #time.sleep(.1)

        
        print("button_value =",button_value)

        to_send1.append(sound_sensor_value)
        to_send2.append(light_sensor_value)
        to_send3.append(ranger_sensor_value)
        to_send4.append(button_value)

        if counter >= 100: 
            now = datetime.datetime.now()
            session.add(SoundSensors(name= 'sound1',date = now ,data=str(to_send1)))
            session.add(LightSensors(name= 'ligth1',date = now ,data=str(to_send2)))
            session.add(UltrasoundSensor(name= 'ultrasonic1',date = now ,data=str(to_send3)))
            session.commit()
            print("SAVED TO DB")
            counter = 0
            to_send1 = []
            to_send2 = []
            to_send3 = []
            to_send4 = []

        counter += 1
        print("-------------")

        #time.sleep(.1)
    except IOError:
        print ("Error")
    except TypeError:
        print ("Error")


'''
@app.route('/1')
def test1():
	#print(dir(db.engine))
	
	result1 = db.engine.execute('SELECT * FROM attribute')
	for rs1 in result1:
		print(rs1)

	result2 = db.engine.execute('SELECT * FROM part')
	for rs2 in result2:
		print(rs2)

	result3 = db.engine.execute('SELECT * FROM part_attributes')
	for rs3 in result3:
		print(rs3)

	result4 = db.engine.execute('SELECT * FROM part_type')
	for rs4 in result4:
		print(rs4)

	result5 = db.engine.execute('SELECT * FROM part_types_attributes')
	for rs5 in result5:
		print(rs5)

	#result6 = db.engine.execute('SELECT * FROM sound_sensors')
	#for rs6 in result6:
	#	print(rs6)

	return render_template('test.html')

@app.route('/2')
def test2():
    return render_template('test2.html')


import os
if __name__ == "__main__":
    # Only for debugging while developing
    #app.run(host='0.0.0.0', debug=True, port=80) 
    app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 1234)))

'''









