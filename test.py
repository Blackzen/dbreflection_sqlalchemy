import time
import datetime
import threading
import random
#from bson.json_util import dumps

#A bunch of import statements which you should ignore for now
from flask import Flask, render_template,  request,jsonify
from flask_socketio import SocketIO, emit,send

from flask_sqlalchemy import SQLAlchemy

from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy import MetaData
from sqlalchemy import Table


from os import path
import sys
import io

from models import sqldb

import grovepi
from picamera import PiCamera

import json

# change to "redis" and restart to cache again

d = path.dirname(__file__)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
app.config['DEBUG'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config["CACHE_TYPE"] = "null"



####NON SQL PART (MONGODB)
'''
def get_db():
    from pymongo import MongoClient
    client = MongoClient('localhost:27017')
    db = client.sensor_data
    return db

def add_data(db,data):
    now = datetime.datetime.now()
    print(now)
    to_send = {
                'time_stamp' : now,
                #"sound_sensor" : data,
                'signals' : data, 
              }
    db.sensors.insert(to_send)  

def get_data(db):
    to_show= []
    counter = 0
    for i in db.sensors.find():
        to_show.append(dumps(i)) 
        counter += 1 
    return to_show 
    #db.sensors.count()

db = get_db() 
'''

#camera instance
#camera = PiCamera()

# Connect the Grove Sound Sensor to analog port A0
# SIG,NC,VCC,GND
sound_sensor = 0

# Connect the Grove Light Sensor to analog port A0
# SIG,NC,VCC,GND
light_sensor = 1

# Connect the Grove Ultrasonic Ranger to digital port D4
# SIG,NC,VCC,GND
ultrasonic_ranger = 4

# Connect the Grove LED to digital port D5
# SIG,NC,VCC,GND
led = 5

button = 6


grovepi.pinMode(sound_sensor,"INPUT")
grovepi.pinMode(light_sensor,"INPUT")
grovepi.pinMode(button,"INPUT")

grovepi.pinMode(led,"OUTPUT")


class TestThreading(object):
    
    
    def __init__(self, interval=0.5):
        self.interval = interval
        self.data = 0
        self.to_send = []
        self.counter = 0
        self.data_amount = 10
        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True
        self.shutdown_flag = threading.Event()
        #self.thread.start()

    def run(self):
        while True:
            # More statements comes here
            #self.data = datetime.datetime.now().__str__()
            #camera.start_preview()
            #camera.capture('/home/pi/Desktop/image%s.jpg' % i)
            #camera.stop_preview()
            # Get sensor values

            #SOUND SENSOR DATA
            sound_sensor_value = grovepi.analogRead(sound_sensor)
            print("sound_sensor_value = %d" %sound_sensor_value)
            #time.sleep(.1)

            #LIGHT SENSOR DATA
            # Calculate resistance of sensor in K
            light_sensor_value = grovepi.analogRead(light_sensor)
            resistance = (float)(1023 - light_sensor_value) * 10 / light_sensor_value
            print("Light_sensor_value = %d resistance = %.2f" %(light_sensor_value,  resistance))
            #time.sleep(.1)

            #ULTRASONIC SENSOR DATA
            ultrasonic_sensor_value  = grovepi.ultrasonicRead(ultrasonic_ranger)
            print("ultrasound_sensor_value = ",ultrasonic_sensor_value)
            #time.sleep(.1)

            button_value = grovepi.digitalRead(button)
            print("button_value =",button_value)
            
            self.data = [sound_sensor_value,light_sensor_value,ultrasonic_sensor_value, button_value]
            time.sleep(self.interval)
            #self.counter +=1
            #print(self.counter)
            #self.to_send.append(self.data)
            #if (self.counter >= self.data_amount):
                #add_data(db,self.to_send)
            
            #    self.counter = 0
            #    self.to_send.clear()


'''
POSTGRES = {
    'user': 'postgres',
    'pw': 'postgres',
    'db': 'professional_maintenance',
    'host': 'localhost',
    'port': '5432',
}

#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://DB_USER:postgres@postgres/test'

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
'''
tr = TestThreading()

POSTGRES = {
    'user': 'u733092923_kms',
    'pw': 'kmsnota2018',
    'db': 'u733092923_kms',
    'host': 'sql122.main-hosting.eu',
    'port': '3306',
}
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False 

sqldb.init_app(app)
engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])


Base = automap_base()

insp = reflection.Inspector.from_engine(engine)
print(insp.get_table_names())

# reflect the tables
Base.prepare(engine, reflect=True)

tables = ['access_level', 'attribute', 'camera_images', 'light_sensor', 'part', 'part_attributes', 'part_type', 'part_types_attributes', 'sound_sensor', 'ultrasonic_ranger', 'user']


Attribute,Part = Base.classes.attribute, Base.classes.part

PartAttributes = Base.classes.part_attributes

SoundSensors = Base.classes.sound_sensor

LightSensors = Base.classes.light_sensor

CameraSensors = Base.classes.camera_images

UltrasoundSensor = Base.classes.ultrasonic_ranger


socketio = SocketIO(app)

@socketio.on('message')
def handleMessage(msg):
    print('Mesage: '+ msg)
    send(msg,broadcast=True)

@socketio.on('get_data')
def send_info():
    send(str(tr.data),broadcast=True)

@socketio.on('my_ping')
def ping_pong():
    emit('my_pong')

    
@app.route('/')
def test1():
    tr.shutdown_flag.set()
    tr.join()
    return render_template('index.html')

#/sockets
@app.route('/1')
def root():
	tr.thread.start()
	return render_template('sockets.html')
#/tree
@app.route('/2')
def test2():
    tr.do_run = False
    #print (get_data(db))
    #result1 = sqldb.engine.execute('SELECT * FROM attribute')
    #for rs1 in result1:
    #    print(rs1)

    #result2 = sqldb.engine.execute('SELECT * FROM part')
    #for rs2 in result2:
    #    print(rs2)

    #result3 = sqldb.engine.execute('SELECT * FROM part_attributes')
    #for rs3 in result3:
    #    print(rs3)

    #result4 = sqldb.engine.execute('SELECT * FROM part_type')
    #for rs4 in result4:
    #    print(rs4)

    #result5 = sqldb.engine.execute('SELECT * FROM part_types_attributes')
    #for rs5 in result5:
    #    print(rs5)

    #result6 = db.engine.execute('SELECT * FROM sound_sensors')
    #for rs6 in result6:
    #   print(rs6)
    
    session = Session(engine)
    #u1 = session.query(Attribute)
    #u2 = session.query(Part)
    to_send = []
    q1 = session.query(SoundSensors)
    q2 = session.query(LightSensors)
    q3 = session.query(UltrasoundSensor)

    for obj in q1.all():
        to_send.append(obj)
    for obj in q2.all():
        to_send.append(obj)
    for obj in q3.all():
        to_send.append(obj)

    test={}
    for i in to_send:
    	dt = i.data[1:-1].split(',')
    	#print(dt)
    	#data_array = map(int,dt)
    	#print(type(dt))
    	#print(type(json.loads(i.data)))
    	if i.name not in test:
    		test[i.name] = []
    		test[i.name].append([i.date,dt])
    	else:
    		test[i.name].append([i.date,dt])
    #print(test)

    #names=[]
    #for i in to_send:
    #	if i.name not in names:
    #		names.append(i.name)
    

    	#print(i.date)
    	#print(i.data)
    #print(to_send)
    result = [to_send,test]
    return render_template('demo.html',result= result)

@app.route('/3')
def test3():
	session = Session(engine)
	to_send = []
	q1 = session.query(SoundSensors)
	q2 = session.query(LightSensors)
	q3 = session.query(UltrasoundSensor)
	for obj in q1.all():
		to_send.append(obj)
	for obj in q2.all():
		to_send.append(obj)
	for obj in q3.all():
		to_send.append(obj)
	return render_template('charts.html',result = to_send)

#/json
@app.route("/4")
def endpoint():
	session = Session(engine)
	to_send = []
	q1 = session.query(SoundSensors)
	q2 = session.query(LightSensors)
	q3 = session.query(UltrasoundSensor)
	for obj in q1.all():
		to_send.append(obj)
	for obj in q2.all():
		to_send.append(obj)
	for obj in q3.all():
		to_send.append(obj)
	test = []
	for t in to_send:
		#print(t.name)
		#print(t.date)
		#print(t.data)
		test.append({"name":t.name,"date":t.date,"data":t.data})
	return jsonify(test)

if __name__ == '__main__':
    socketio.run(app)
