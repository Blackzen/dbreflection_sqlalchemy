from flask_sqlalchemy import SQLAlchemy
import datetime

sqldb = SQLAlchemy()

class BaseModel(sqldb.Model):
    """Base data model for all objects"""
    __abstract__ = True

    def __init__(self, *args):
        super().__init__(*args)

    def __repr__(self):
        """Define a base way to print models"""
        return '%s(%s)' % (self.__class__.__name__, {
            column: value
            for column, value in self._to_dict().items()
        })

    def json(self):
        """
                Define a base way to jsonify models, dealing with datetime objects
        """
        return {
            column: value if not isinstance(value, datetime.date) else value.strftime('%Y-%m-%d')
            for column, value in self._to_dict().items()
        }

'''
class User(BaseModel, sqldb.Model):
	__tablename__ = 'user'
	id = sqldb.Column(sqldb.Integer, primary_key = True)
    name = sqldb.Column(sqldb.String, primary_key = True)
    password = sqldb.Column(sqldb.String, primary_key = True)
    role = sqldb.Column(sqldb.String, primary_key = True)
    surname = sqldb.Column(sqldb.String, primary_key = True)
    name = sqldb.Column(sqldb.String, primary_key = True)
'''

class User(sqldb.Model):
    __tablename__ = 'user'
    username = sqldb.Column(sqldb.Integer, primary_key=True)
    password = sqldb.Column(sqldb.String(50), nullable=False)
    role = sqldb.Column(sqldb.String(50), nullable=False)
    surname = sqldb.Column(sqldb.String(50), nullable=False)
    name = sqldb.Column(sqldb.String(50), nullable=False)
    
class Settings_ml(sqldb.Model):
    __tablename__ = 'settings_ml'
    type_id = sqldb.Column(sqldb.Integer, primary_key=True)
    value = sqldb.Column(sqldb.String, nullable=False)

class Part_relation_type(sqldb.Model):
    __tablename__ = 'part_relation_type_id'
    part_relation_type_id = sqldb.Column(sqldb.Integer, primary_key=True)

class Part_realtion(sqldb.Model):
    __tablename__ = 'part_relation'
    part_side_a = sqldb.Column(sqldb.Integer, nullable=False)
    part_side_b = sqldb.Column(sqldb.Integer, nullable=False)
    part_relation_type_id = sqldb.Column(sqldb.Integer, primary_key=True)

class Atribute(sqldb.Model):
    __tablename__ = 'atribute'
    atribute_id = sqldb.Column(sqldb.Integer, primary_key=True)
    name = sqldb.Column(sqldb.String(50), nullable=False)

class Part_type(sqldb.Model):
    __tablename__ = 'part_type'
    part_type_id = sqldb.Column(sqldb.Integer, primary_key=True)
    name = sqldb.Column(sqldb.String(50), nullable=False)

class Part(sqldb.Model):
    __tablename__ = 'part'
    part_id = sqldb.Column(sqldb.Integer, primary_key=True)
    name = sqldb.Column(sqldb.String(50), nullable=False)
