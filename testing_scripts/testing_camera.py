from picamera import PiCamera
from time import sleep

camera = PiCamera()

#camera.start_preview()
#camera.start_preview(alpha=200)
#sleep(10)
#camera.stop_preview()

#camera.start_preview()
#sleep(5)
#camera.capture('/home/pi/Desktop/image.jpg')
#camera.stop_preview()

camera.start_preview()
for i in range(5):
    sleep(1)
    camera.capture('/home/pi/Desktop/image%s.jpg' % i)
camera.stop_preview()

#camera.start_preview()
#camera.start_recording('/home/pi/Desktop/video.h264')
#sleep(5)
#camera.stop_recording()
#camera.stop_preview()

#camera.resolution = (2592, 1944)
#camera.framerate = 15
#camera.start_preview()
#sleep(5)
#camera.capture('/home/pi/Desktop/max.jpg')
#camera.stop_preview()

#camera.start_preview()
#camera.annotate_text = "Hello world!"
#sleep(5)
#camera.capture('/home/pi/Desktop/text.jpg')
#camera.stop_preview()

#camera.start_preview()
#for effect in camera.IMAGE_EFFECTS:
#    camera.image_effect = effect
#    camera.annotate_text = "Effect: %s" % effect
#    sleep(5)
#camera.stop_preview()