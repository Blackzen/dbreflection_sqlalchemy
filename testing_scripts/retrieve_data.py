from flask import Flask
from flask import jsonify
app = Flask(__name__)


def get_db():
    from pymongo import MongoClient
    client = MongoClient('localhost:27017')
    db = client.sensor_data
    return db

def get_data(db):

    return db.sensors.find({})
    #return db.sensors.count()


db = get_db() 

@app.route('/')
def hello():
	print(db.sensors.count())
	temp = get_data(db)
	#print(temp)
	response = []
	for i in temp:
		response.append(i)
	return str(response)

if __name__ == '__main__':
    app.run()