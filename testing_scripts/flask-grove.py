import time
import datetime
import threading
import random

#import grovepi
import numpy as np

#A bunch of import statements which you should ignore for now
from flask import Flask, render_template,  request
from flask_socketio import SocketIO, emit,send 

class TestThreading(object):
	
    def __init__(self, interval=0.001):
    	self.interval = interval
    	self.data = 0
    	thread = threading.Thread(target=self.run, args=())
    	thread.daemon = True
    	thread.start()

    def run(self):
        grovepi.pinMode(button,"INPUT")
        while True:
            try:
                # More statements comes here
                #self.data = datetime.datetime.now().__str__()
                self.data = grovepi.digitalRead(button)
                time.sleep(self.interval)
            except KeyboardInterrupt:
                grovepi.digitalWrite(buzzer,0)
                grovepi.digitalWrite(led,0)    
                break
            except IOError:
                print ("Error")

tr = TestThreading()



app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
DEBUG = True
socketio = SocketIO(app)

@socketio.on('message')
def handleMessage(msg):
	print('Mesage: '+ msg)
	send(msg,broadcast=True)

@socketio.on('get_data')
def send_info():
	send(str(tr.data),broadcast=True)

@socketio.on('my_ping')
def ping_pong():
    emit('my_pong')

if __name__ == '__main__':
    socketio.run(app)
