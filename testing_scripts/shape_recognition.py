'''
import numpy as np
import cv2

img = cv2.imread('shapes.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

ret,thresh = cv2.threshold(gray,127,255,1)

_,contours,_ = cv2.findContours(thresh,1,2)

for cnt in contours:
    approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
    print (len(approx))
    if len(approx)==5:
        print ("pentagon")
        cv2.drawContours(img,[cnt],0,255,-1)
    elif len(approx)==3:
        print ("triangle")
        cv2.drawContours(img,[cnt],0,(0,255,0),-1)
    elif len(approx)==4:
        print ("square")
        cv2.drawContours(img,[cnt],0,(0,0,255),-1)
    elif len(approx) == 9:
        print ("half-circle")
        cv2.drawContours(img,[cnt],0,(255,255,0),-1)
    elif len(approx) > 15:
        print ("circle")
        cv2.drawContours(img,[cnt],0,(0,255,255),-1)

cv2.imshow('img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
'''
import numpy as np
import cv2
img = cv2.imread('index.png',0)
width,height = img.shape[:2]
img2 = np.zeros((width,height,3))

cv2.imshow('read img',img)
ret,thresh = cv2.threshold(img,127,255,1)
_,contours,_ = cv2.findContours(thresh,1,2)

print("amount"+ str(len(contours)))

for cnt in contours:
    approx = cv2.approxPolyDP(cnt,0.005*cv2.arcLength(cnt,True),True)
    print (len(approx))
    if len(approx)==5:
        pass
        print ("pentagon")
        cv2.drawContours(img,[cnt],0,255,-1)
    elif len(approx)==3:
        pass
        print ("triangle")
        cv2.drawContours(img2,[cnt],0,(0,255,0),-1)
    elif len(approx)==4:
        pass
        print ("square")
        cv2.drawContours(img2,[cnt],0,(0,0,255),-1)
    elif len(approx) == 9:
        pass
        #print ("half-circle")
        #cv2.drawContours(img2,[cnt],0,(255,255,0),3)
    elif len(approx) > 15 :
        print (len(approx))
        print ("circle")
        cv2.drawContours(img2,[cnt],0,(0,255,255),3)

cv2.imshow('img',img2)
cv2.waitKey(0)
#This is the right way to do things
#try:
  #Some stuff might raise an IO exception
    #except KeyboardInterrupt:
        #break
  #This won't catch KeyboardInterupt