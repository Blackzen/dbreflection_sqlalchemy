import cv2
import numpy as np
 
# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture(0)
 
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video stream or file")
 
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
  	im_bw = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
  	# Display the resulting frame
  	
  	#cv2.imshow('Frame',im_bw)
  	
  	ret,thresh = cv2.threshold(im_bw,127,255,1)
  	_,contours,_ = cv2.findContours(thresh,1,2)

  	for cnt in contours:
  		approx = cv2.approxPolyDP(cnt,0.005*cv2.arcLength(cnt,True),True)
  		print (len(approx))
  		if len(approx) > 40 :
  			#pass
  			print (len(approx))
  			print ("circle")
  			cv2.drawContours(frame,[cnt],0,(0,255,255),3)

  	cv2.imshow('Frame',frame)

  	# Press Q on keyboard to  exit
  	if cv2.waitKey(25) & 0xFF == ord('q'):
  		break

  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
 
# Closes all the frames
cv2.destroyAllWindows()


'''
import cv2
import numpy as np
 
# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture(0)
 
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video stream or file")
 
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
    # Display the resulting frame
    im_bw = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    cv2.imshow('Frame',im_bw)
 
    ret,thresh = cv2.threshold(im_bw,127,255,1)
    _,contours,_ = cv2.findContours(thresh,1,2)

    for cnt in contours:
    	approx = cv2.approxPolyDP(cnt,0.005*cv2.arcLength(cnt,True),True)
    	print (len(approx))
    	if len(approx)==5:
    		pass
    		#print ("pentagon")
    		#cv2.drawContours(frame,[cnt],0,255,-1)
    	elif len(approx)==3:
    		pass
    		#print ("triangle")
    		#cv2.drawContours(frame,[cnt],0,(0,255,0),-1)
    	elif len(approx)==4:
    		pass
    		#print ("square")
    		#cv2.drawContours(frame,[cnt],0,(0,0,255),-1)
    	elif len(approx) > 300 :
    		pass
    		print (len(approx))
    		print ("circle")
    		cv2.drawContours(frame,[cnt],0,(0,255,255),3)
	    	# Press Q on keyboard to  exit
    #if cv2.waitKey(25) & 0xFF == ord('q'):
    #  break
    	#elif len(approx) == 9:
    	#	pass
	        #print ("half-circle")
	        #cv2.drawContours(img2,[cnt],0,(255,255,0),3)
 
  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
 
# Closes all the frames
cv2.destroyAllWindows()
'''