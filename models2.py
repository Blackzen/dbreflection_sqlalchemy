from flask_sqlalchemy import SQLAlchemy
import datetime

db = SQLAlchemy()

class BaseModel(db.Model):
    """Base data model for all objects"""
    __abstract__ = True

    def __init__(self, *args):
        super().__init__(*args)

    def __repr__(self):
        """Define a base way to print models"""
        return '%s(%s)' % (self.__class__.__name__, {
            column: value
            for column, value in self._to_dict().items()
        })

    def json(self):
        """
                Define a base way to jsonify models, dealing with datetime objects
        """
        return {
            column: value if not isinstance(value, datetime.date) else value.strftime('%Y-%m-%d')
            for column, value in self._to_dict().items()
        }

'''
class User(BaseModel, db.Model):
	__tablename__ = 'user'
	id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, primary_key = True)
    password = db.Column(db.String, primary_key = True)
    role = db.Column(db.String, primary_key = True)
    surname = db.Column(db.String, primary_key = True)
    name = db.Column(db.String, primary_key = True)
'''

class User(db.Model):
    __tablename__ = 'user'
    username = db.Column(db.Integer, primary_key=True)
    password = db.Column(db.String(50), nullable=False)
    role = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    
class Settings_ml(db.Model):
    __tablename__ = 'settings_ml'
    type_id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String, nullable=False)

class Part_relation_type(db.Model):
    __tablename__ = 'part_relation_type_id'
    part_relation_type_id = db.Column(db.Integer, primary_key=True)

class Part_realtion(db.Model):
    __tablename__ = 'part_relation'
    part_side_a = db.Column(db.Integer, nullable=False)
    part_side_b = db.Column(db.Integer, nullable=False)
    part_relation_type_id = db.Column(db.Integer, primary_key=True)

class Atribute(db.Model):
    __tablename__ = 'atribute'
    atribute_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)

class Part_type(db.Model):
    __tablename__ = 'part_type'
    part_type_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)

class Part(db.Model):
    __tablename__ = 'part'
    part_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
